package com.bitcola.exchange.launchpad.constant;

/**

 * @create 2019-05-18 10:17
 **/
public interface ColaIsoUnlockType {
    String AUTO = "AUTO";
    String INVITER = "INVITER";
    String TIME = "TIME";
}
