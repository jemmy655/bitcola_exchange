package com.bitcola.exchange.launchpad.vo;

import lombok.Data;

/**

 * @create 2019-03-14 10:54
 **/
@Data
public class Community {
    String platform;
    String url;
    String iconClass;
}
