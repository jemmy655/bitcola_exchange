package com.bitcola.chain.entity;

import lombok.Data;

/**

 * @create 2019-01-19 16:11
 **/
@Data
public class Message {
    boolean isSuccess = true;
    String errorMsg;
    Object data;
}
