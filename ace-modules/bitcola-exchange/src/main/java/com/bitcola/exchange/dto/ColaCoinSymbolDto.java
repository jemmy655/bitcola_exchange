package com.bitcola.exchange.dto;

import lombok.Data;

/**

 * @create 2019-02-16 12:44
 **/
@Data
public class ColaCoinSymbolDto {
    String coinCode;
    String symbol;
}
