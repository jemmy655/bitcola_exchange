package com.bitcola.exchange.security.community.entity;

import lombok.Data;

/**
 * 图片
 *

 * @create 2018-09-13 16:56
 **/
@Data
public class ImageEntity {

    private String source;
    private String small;
    private String type;
}
