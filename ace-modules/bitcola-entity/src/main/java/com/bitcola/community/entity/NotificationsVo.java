package com.bitcola.community.entity;

import lombok.Data;

/**

 * @create 2018-11-05 11:18
 **/
@Data
public class NotificationsVo extends NotificationsEntity {

    String fromUsername;
    String fromUserNickName;
    String fromUserAvatar;


}
