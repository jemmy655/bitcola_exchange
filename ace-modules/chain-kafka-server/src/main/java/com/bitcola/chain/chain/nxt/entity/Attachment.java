package com.bitcola.chain.chain.nxt.entity;

import lombok.Data;

/**

 * @create 2019-03-12 10:37
 **/
@Data
public class Attachment {
    String message;
    EncryptedMessage encryptedMessage;
}
