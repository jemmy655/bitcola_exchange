package com.bitcola.chain.chain.xem.entity;

import lombok.Data;

/**

 * @create 2018-11-23 18:45
 **/
@Data
public class Hash {
    String data;
}
