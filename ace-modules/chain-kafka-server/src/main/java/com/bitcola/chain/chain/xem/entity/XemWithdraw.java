package com.bitcola.chain.chain.xem.entity;

import lombok.Data;

/**

 * @create 2018-11-26 11:54
 **/
@Data
public class XemWithdraw {
    Transaction transaction;
    String privateKey;
}
