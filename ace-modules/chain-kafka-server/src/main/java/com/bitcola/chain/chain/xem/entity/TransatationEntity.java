package com.bitcola.chain.chain.xem.entity;

import lombok.Data;

/**

 * @create 2018-11-23 18:31
 **/
@Data
public class TransatationEntity {
    Meta meta;
    Transaction transaction;
}
