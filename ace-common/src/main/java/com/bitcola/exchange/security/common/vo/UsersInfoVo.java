package com.bitcola.exchange.security.common.vo;

import lombok.Data;

/**

 * @create 2018-11-09 10:58
 **/
@Data
public class UsersInfoVo {
    String username;
    String nickName;
    String id;
    String sign;
    String avatar;
}
